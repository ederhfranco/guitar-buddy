import { Component, Input, OnInit } from '@angular/core';
import { MetronomeEngineService } from '../services/metronome-engine.service';

@Component({
  selector: 'app-metronome',
  templateUrl: './metronome.component.html',
  styleUrls: ['./metronome.component.scss']
})
export class MetronomeComponent implements OnInit {

  @Input()
  beatsPerMinute: number = 90;

  running = false;

  constructor(private engine: MetronomeEngineService) {
  }

  ngOnInit() {
  }

  onChange(): void {
    this.engine.setBpm(this.beatsPerMinute);
  }

  formatLabel(value: number) {
    this.beatsPerMinute = value;
    return value + " BPM";
  }

  start(): void {
    if (!this.running) {
      this.running = true;
      this.engine.start();
    }
  }

  stop() {
    this.running = false;
    this.engine.stop();
  }

}
