export class ExerciseItem {
    id?: number;
    bpm: number = 0;
    breakSeconds: number = 0;
    hasBreak: boolean = this.breakSeconds > 0;
    durationSeconds: number = 0;
    durationMinutes: number = 0;
    get totalSeconds(): number{
        const secondsFromDurationMinutes = this.durationMinutes > 0 ? this.durationMinutes * 60 : 0;
        const secondsFromDurationSeconds = this.durationSeconds > 0 ? this.durationSeconds : 0;
        return secondsFromDurationMinutes + secondsFromDurationSeconds;
    }
    secondsLeft: number = 0;
    get secondsLeftInMiliseconds(): number { return this.secondsLeft * 1000 };

    breakSecondsLeft: number = 0;
    playingNow: boolean = false;
    alreadyBeenPlayed: boolean = false;
    title?: string;
}
