import { TestBed } from '@angular/core/testing';

import { MetronomeEngineService } from './metronome-engine.service';

describe('MetronomeEngineService', () => {
  let service: MetronomeEngineService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MetronomeEngineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
