import { Injectable } from '@angular/core';
const Tone = require('tone');
const StartAudioContext = require('startaudiocontext');

@Injectable({
  providedIn: 'root'
})
export class MetronomeEngineService {

  private beatsPerMinute = 90;

  private itWasSet = false;

  constructor() {
  }

  setup(): void {
    if (!this.itWasSet) {
      StartAudioContext(Tone.context, document.documentElement);
      if (Tone.context.state !== 'running') {
        Tone.context.resume();
      }

      this.setBpm(this.beatsPerMinute);

      const tickSynth = this.buildTick();

      Tone.Transport.scheduleRepeat((time: any) => {
        tickSynth.triggerAttackRelease("B5", "16n", time);
      }, '4n');

      this.itWasSet = true;
    }
  }

  setBpm(bpm: number): void {
    this.beatsPerMinute = bpm;
    Tone.Transport.bpm.value = bpm;
    this.buildTick();
  }

  start(): void {
    this.setup();
    Tone.Transport.start();
  }

  stop(): void {
    Tone.Transport.stop();
    Tone.Transport.position = 0;
  }

  private buildTick(): any {
    const tickSynth =  new Tone.Synth().toMaster();
    tickSynth.oscillator.type = 'sine';
    tickSynth.envelope.attack = 0;
    tickSynth.envelope.decay = 0.05;
    tickSynth.envelope.sustain = 0;
    tickSynth.envelope.release = 0.001;

    tickSynth.toMaster();

    return tickSynth;
  }

}
