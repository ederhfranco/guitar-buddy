import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingPlayComponent } from './training-play.component';

describe('TrainingPlayComponent', () => {
  let component: TrainingPlayComponent;
  let fixture: ComponentFixture<TrainingPlayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrainingPlayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingPlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
