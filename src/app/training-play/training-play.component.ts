import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription, timer } from 'rxjs';
import { ExerciseItem } from '../models/exercise-item';
import { MetronomeEngineService } from '../services/metronome-engine.service';

@Component({
  selector: 'app-training-play',
  templateUrl: './training-play.component.html',
  styleUrls: ['./training-play.component.scss']
})
export class TrainingPlayComponent implements OnInit {

  running = false;
  trainingWasStarted = false;

  exercises: ExerciseItem[] = [];

  exerciseForm: FormGroup;

  exerciseSubscribe = new Subscription();
  breakSubscribe = new Subscription();
  breakTimerSubscribe = new Subscription();
  subscribeSecondsSource = new Subscription();

  get totalExercises() { return this.exerciseForm.controls['totalExercises'].value }
  get exerciseMinutes() { return this.exerciseForm.controls['exerciseMinutes'].value }
  get exerciseSeconds() { return this.exerciseForm.controls['exerciseSeconds'].value }
  get bpm() { return this.exerciseForm.controls['bpm'].value }
  get breakSeconds() { return this.exerciseForm.controls['breakSeconds'].value }

  constructor(
    private formBuilder: FormBuilder,
    private metronomeEngine: MetronomeEngineService,
  ) {
    this.exerciseForm = this.formBuilder.group({
      totalExercises: [0, Validators.required],
      exerciseMinutes: [0, Validators.required],
      exerciseSeconds: [0, Validators.required],
      bpm: [0, Validators.required],
      breakSeconds: [0, Validators.required]
    });
  }

  ngOnInit(): void {
    
  }

  handleTraining() {
    const itemNotPlayed = this.exercises.filter((item) => item.alreadyBeenPlayed === false);
    const itemToPlay = itemNotPlayed !== undefined && itemNotPlayed.length > 0 ? itemNotPlayed[0] : undefined;

    if (itemToPlay) {
        this.processExercise(itemToPlay);
    } else {
      setTimeout(() => {
        this.pauseTraining();
      });
    }
  }

  playTraining() {
    this.running = true;    
    this.handleTraining();
  }

  pauseTraining() {
    this.running = false;
    this.metronomeEngine.stop();
    this.exerciseSubscribe.unsubscribe();
    this.exerciseSubscribe.unsubscribe();
    this.breakSubscribe.unsubscribe();
    this.breakTimerSubscribe.unsubscribe();
    this.subscribeSecondsSource.unsubscribe();
  }

  private processExercise(itemToPlay: ExerciseItem) {
    // STARTS METRONOME
    console.log(`--- Starting Exercise ${itemToPlay.title} - BPM: ${itemToPlay.bpm}`);
    itemToPlay.playingNow = true;
    this.metronomeEngine.setBpm(itemToPlay.bpm);
    this.metronomeEngine.start();
    
    const exerciseSource = timer(itemToPlay.secondsLeft * 1000);
    this.exerciseSubscribe = exerciseSource.subscribe(() => {

      // STOPS METRONOME
      console.log(`--- Breaking Exercise ${itemToPlay.title} - BPM: ${itemToPlay.bpm}`);
      this.metronomeEngine.stop();

      const breakSource = timer(itemToPlay.breakSeconds * 1000);
      this.breakSubscribe = breakSource.subscribe(() => {
        console.log(`--- Finishing Exercise ${itemToPlay.title}`);
        this.handleTraining();
      });

      const breakSourceTimer = timer(1000, 1000);
      this.breakTimerSubscribe = breakSourceTimer.subscribe(() => {
        console.log(`Exercise ${itemToPlay.title} - Break Time left: ${itemToPlay.breakSecondsLeft}`);
        itemToPlay.breakSecondsLeft = itemToPlay.breakSecondsLeft - 1;
        if (itemToPlay.breakSecondsLeft == 0) {
          this.breakTimerSubscribe.unsubscribe();
        }
      });
    });

    // STARTS EXERCISE TIMER
    const secondsSource = timer(1000, 1000);
    this.subscribeSecondsSource = secondsSource.subscribe(() => {
      console.log(`Exercise ${itemToPlay.title} - Time left: ${itemToPlay.secondsLeft}`);
      itemToPlay.secondsLeft = itemToPlay.secondsLeft - 1;
      if (itemToPlay.secondsLeft == 0) {
        itemToPlay.playingNow = false;
        itemToPlay.alreadyBeenPlayed = true;
        this.subscribeSecondsSource.unsubscribe();
      }
    });

    
  }

  buildTraning() {
    this.exercises = [];

    for (let index = 1; index <= this.totalExercises; index++) {
      const exercise = new ExerciseItem();
      exercise.id = index;
      exercise.bpm = this.bpm;
      exercise.hasBreak = true;
      exercise.breakSeconds  = this.breakSeconds;
      exercise.durationSeconds = this.exerciseSeconds;
      exercise.durationMinutes = this.exerciseMinutes;
      exercise.secondsLeft = exercise.totalSeconds;
      exercise.breakSecondsLeft = exercise.breakSeconds;
      exercise.alreadyBeenPlayed = false;
      exercise.title = `Exercise ${index}`;

      this.exercises.push(exercise);
    }
  }

}
